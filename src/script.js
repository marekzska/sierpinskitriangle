document.getElementById("root").style.width = document.getElementById("root").offsetHeight + "px";
var tips = [{ x: 0, y: 0 }, { x: 100, y: 0 }, { x: 50, y: 100 }]
for (let i = 0; i < tips.length; i++) {
  var square = document.createElement("div");
  square.style.width = "5px";
  square.style.height = "5px";
  square.style.position = "absolute";
  square.style.bottom = tips[i].y + "%";
  square.style.left = tips[i].x + "%";
  square.style.backgroundColor = "black";
  document.getElementById("root").appendChild(square);
}

var points = [];
var randomHeight = Math.floor(Math.random() * 100);
points.push({x: (randomHeight / Math.sin(60 * (Math.PI / 180))) * Math.sin(30 * (Math.PI / 180)) , y: randomHeight})

for(i = 0; i< 100000; i++){
  var square = document.createElement("div");
  square.style.width = "2px";
  square.style.height = "2px";
  square.style.position = "absolute";
  square.style.bottom = points.at(-1).y + "%";
  square.style.left = points.at(-1).x + "%";
  square.style.backgroundColor = "white";
  square.style.borderRadius = "50%";
  document.getElementById("root").appendChild(square);
  var tip = Math.floor(Math.random() * 3);
  points.push({x: (points.at(-1).x + tips[tip].x) / 2 , y: (points.at(-1).y + tips[tip].y) / 2 })
}



console.log((randomHeight / Math.sin(60 * (Math.PI / 180))) * Math.sin(30 * (Math.PI / 180)))